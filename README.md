# Type Design Bundle

Folder for the Type Design Bundle classes.

--> marte.verhaegen@gmail.com

--> @LettersBundle24, deathToMonotype

## Final assignment

- One digital “font” (min. 6 letters). This can be based on one of your typecookers, your ugly drawing, the pixel font or something completely different.
- A digital presentation of your letters, in the form of an “Instagram post”. You choose the format (static/ motion/ carousel) and proportions of the image(s)/ video(s).

**Before 17.15:** Send your font to marte.verhaegen@gmail.com, upload your post(s) to the instagram account with a small description of your letters and your name.

## 26/03
#### Exporting and Installing Fonts
Most times, you don't want to directly install your font on your computer, except if it is **completely finished**, because installing the same font twice will lead to problems and caching-issues. This is why most font editors have an option to "test install" a font. This does not install the font on your computer, but only makes a temporary file that you can use in your programs.

**TIP:** If you make a folder named "Document Fonts" next to your inDesign file, you can use all the `.ttf` or `.otf` fonts inside the folder in your inDesign file without installing them. Might require re-opening your inDesign file. [(Under "document intalled fonts")](https://helpx.adobe.com/indesign/using/using-fonts.html)

#### How to export your font:
- Fontra:
	- Sadly enough, it is not possible to export a font in Fontra yet. Whenever you are ready, you can send your `.ufo` file to me (or to a classmate), and we'll export it through RoboFont.
- RoboFont:
	- [Option 1] File > Test Install. This installs your letters temporary on your computer. Very handy if you still want to adjust your letters afterwards. [(link)](https://robofont.com/documentation/tutorials/using-test-install/)
	- [Option 2] File > Generate Font. This will generate an `.otf` or `.ttf` file that you can install on your computer. [(link)](https://robofont.com/documentation/tutorials/generating-fonts/)
- Glyphs:
	- File > Export > OTF. Keep all the settings as they are. You can choose under "Export Destination" between installing it on your computer or test install. [(link)](https://glyphsapp.com/learn/testing-your-fonts-in-adobe-apps)

## 19/03
#### How to make a Variable Font:
- Fontra:
	- Download the `.designspace` file from the Resources folder
	- Modify with the names of your own fonts
	- Open in Fontra
- RoboFont: 
	- Download [Mechanic 2](https://robofontmechanic.com)
	- In Robofont > Extensions > Mechanic 2, install Batch and DesignspaceEditor
	- See screencast in Resources folder
- Glyphs: [Creating a variable font](https://glyphsapp.com/learn/creating-a-variable-font)


## 12/03
Assignment: Start from one of your neighbour's drawings and make the ugliest letters. Don't just copy and adjust from your neighbour, but go a stap further. This can be anything, but the result should still be legible-ish.

#### Glyph Editors:
- [Download Robofont 4.4](https://robofont.com/announcements/RoboFont-4.4/) (bottom page)
- [Download Fontra](https://github.com/googlefonts/fontra-pak/actions/runs/8242256099)

## Useful Links
- [Typecooker](http://typecooker.com)
- [Extra tips by James Edmondson](https://ohnotype.co/blog/tagged/teaching)

### Type Foundries
- [OHno Type](https://ohnotype.co)
- [Contrast Foundry](https://contrastfoundry.com)
- [Future Fonts](https://www.futurefonts.xyz)
- [NaN](https://www.nan.xyz)
- [Dinamo](https://abcdinamo.com/studio)
- [Underware](https://www.underware.nl)
- [Typotheque](https://www.typotheque.com)
